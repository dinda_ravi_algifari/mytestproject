import { createRouter, createWebHistory } from 'vue-router';
import Home from '../components/Home.vue';
import LoginForm from '../components/Login.vue';
import VendorList from '../components/VendorList.vue';
import VendorForm from '../components/VendorForm.vue';
import SignupForm from '../components/Signup.vue';

const routes = [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/loginForm',
      name: 'LoginForm',
      component: LoginForm
    }
    ,
    {
      path: '/signupForm',
      name: 'SignupForm',
      component: SignupForm
    }
    ,
    {
      path: '/vendorList',
      name: 'VendorList',
      component: VendorList
    }
    ,
    {
      path: '/vendorForm',
      name: 'VendorForm',
      component: VendorForm
    }
  ];
  
  const router = createRouter({
    history: createWebHistory(),
    routes
  });
  
  export default router;
  