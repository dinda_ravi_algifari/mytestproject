## Create WebApi
```
dotnet new webapi -o WebApi --framework "net6.0"
```


```
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design -v 6.0.9
dotnet add package Microsoft.EntityFrameworkCore.Design -v 6.0.9
dotnet add package Microsoft.EntityFrameworkCore.SqlServer -v 6.0.9
dotnet tool uninstall -g dotnet-aspnet-codegenerator
dotnet tool install -g dotnet-aspnet-codegenerator
dotnet add package Microsoft.AspNetCore.Authentication.JwtBearer --version 6.0.9

dotnet ef migrations add InitialCreate
dotnet ef database update


dotnet-aspnet-codegenerator controller -name TodoItemsController -async -api -m TodoItem -dc TodoContext -outDir Controllers
dotnet aspnet-codegenerator controller -name VendorController -async -api -m Vendor -dc AppDbContext --relativeFolderPath Controllers

```