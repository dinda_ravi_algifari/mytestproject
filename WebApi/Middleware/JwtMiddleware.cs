using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Authentication;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebApi.Models;

namespace WebApi.Middleware
{

    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly JwtOptions _jwtOptions;
        private readonly IServiceProvider _serviceProvider;

        public JwtMiddleware(RequestDelegate next, IOptions<JwtOptions> jwtOptions, IServiceProvider serviceProvider)
        {
            _next = next;
            _jwtOptions = jwtOptions.Value;
            _serviceProvider = serviceProvider;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            if (token != null)
                AttachUserToContext(context, token);

            await _next(context);
        }

        private void AttachUserToContext(HttpContext context, string token)
        {
            try
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    // Mendapatkan instance DbContext dari scope
                    var dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();
 
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.UTF8.GetBytes(_jwtOptions.SecretKey);
                    var symmetricSecurityKey = new SymmetricSecurityKey(key);
                    tokenHandler.ValidateToken(token, new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = symmetricSecurityKey,
                        ValidateIssuer = true,
                        ValidIssuer = _jwtOptions.Issuer,
                        ValidateAudience = true,
                        ValidAudience = _jwtOptions.Audience,
                        ClockSkew = TimeSpan.Zero
                    }, out var validatedToken);

                    var jwtToken = (JwtSecurityToken)validatedToken;
                    var Email = jwtToken.Claims.SingleOrDefault(x => x.Type == ClaimTypes.Email).Value;
                    // Anda dapat melakukan pengecekan tambahan atau memuat data pengguna dari database
                    // Misalnya, melakukan pengecekan apakah pengguna ada dalam database
                    if(Email != null)
                    {
                        var user = dbContext.Users.SingleOrDefault(e=> e.Email.Equals(Email));
                        if(user == null) { throw new AuthenticationException("User belum terdaftar"); }
                        // Simpan username pengguna di HttpContext.Items
                        context.Items["UserEmail"] = Email;
                    }else
                    {
                        throw new Exception("Email tidak di temukan");
                    }
                }
            }
            catch(Exception e)
            {
                // Token tidak valid, abaikan atau tangani sesuai kebutuhan Anda
                throw new AuthenticationException(e.InnerException.Message??e.Message);
            }
        }

        public (int,string) ExtractKeyFromJwt(string token)
        {
            var jwtHandler = new JwtSecurityTokenHandler();

            // Membaca token JWT dan menguraikannya menjadi objek JwtSecurityToken
            var jwtToken = jwtHandler.ReadJwtToken(token);

            // Mendapatkan kunci dari token JWT
            var key = jwtToken.SigningKey;

            // Mengembalikan kunci dalam bentuk string
            var keyString = key.ToString();

            return (0,keyString);
        }
    }
}
