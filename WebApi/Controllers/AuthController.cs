using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebApi.Middleware;
using WebApi.Models;
using WebApi.ViewModels;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly AppDbContext _context;
        private readonly JwtOptions _jwtOptions;

        public AuthController(IConfiguration configuration, AppDbContext context, IOptions<JwtOptions> jwtOptions)
        {
            _configuration = configuration;
            _context = context;
            _jwtOptions = jwtOptions.Value;
        }

        [AllowAnonymous]
        [HttpPost("signup")]
        public IActionResult Signup([FromBody] UserViewModel user)
        {
            // Cek apakah username sudah digunakan
            if (_context.Users.Any(u => u.Email == user.Email))
            {
                return BadRequest(new { Message = "Email already exists" });
            }

            var userModel = new User{
                Email = user.Email,
                Password = user.Password,
                CreatedAt = DateTime.UtcNow,
                CreatedBy = 0
            };
            // Tambahkan pengguna baru ke database
            _context.Users.Add(userModel);
            _context.SaveChanges();

            // Berhasil mendaftarkan pengguna, kembalikan respons OK
            return Ok(new { Message = "Signup successful" });
        }

        [AllowAnonymous]
        [HttpPost("signin")]
        public IActionResult Signin([FromBody] UserViewModel user)
        {
            var foundUser = _context.Users.FirstOrDefault(u => u.Email == user.Email && u.Password == user.Password);
            if (foundUser == null)
            {
                return Unauthorized(new { Message = "Invalid username or password" });
            }

            // Ambil konfigurasi JWT dari appsettings.json
            var jwtConfig = _configuration.GetSection("JwtConfig");
            var secretKey = _jwtOptions.SecretKey;
            var issuer = _jwtOptions.Issuer;
            var audience = _jwtOptions.Audience;
            var expirationInMinutes = _jwtOptions.ExpirationInMinutes;

            // Buat token JWT
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(secretKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Email, foundUser.Email),
                }),
                Expires = DateTime.UtcNow.AddMinutes(expirationInMinutes),
                Issuer = issuer,
                Audience = audience,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)

            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // Autentikasi berhasil, kembalikan token sebagai respons
            return Ok(new { Token = tokenString });
        }


    }
}