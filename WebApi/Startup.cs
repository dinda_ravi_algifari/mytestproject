using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;                                                                                                                                                                                        
using Swashbuckle.AspNetCore.Swagger;
using WebApi.Middleware;
using WebApi.Models;
using Microsoft.OpenApi.Models;

namespace WebApi
{
    public class Startup
    {
        public IConfiguration _configuration { get; }
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            // Konfigurasi database PostgreSQL
            services.AddScoped<AppDbContext>();
            services.AddDbContext<AppDbContext>(options =>
                options.UseNpgsql(_configuration.GetConnectionString("DefaultConnection")));

            // Konfigurasi JWT authentication
            var jwtOptions = _configuration.GetSection("JwtConfig").Get<JwtOptions>();
            services.Configure<JwtOptions>(_configuration.GetSection("JwtConfig"));
            services.Configure<JwtOptions>(options =>
                    {
                        options.SecretKey = _configuration["JwtConfig:SecretKey"];
                    });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidIssuer = jwtOptions.Issuer,
                    ValidAudience = jwtOptions.Audience,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.SecretKey))
                };
            });

            services.AddEndpointsApiExplorer();
            // services.AddSwaggerGen();
            
            // Di dalam method ConfigureServices di file Startup.cs
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API Vendor", Version = "v1" });

                // Konfigurasi otorisasi di Swagger
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header menggunakan skema Bearer. Contoh: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new List<string>()
                    }
                });
            });


            // Registrasi service untuk keperluan login dan signup
            // services.AddScoped<IAuthService, AuthService>();
        }
        // public void ConfigureServices(IServiceCollection services)
        // {
        //     // Konfigurasi layanan Anda di sini

        //     // Konfigurasi autentikasi JWT
        //     var jwtOptions = _configuration.GetSection("JwtConfig").Get<JwtOptions>();
        //     services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        //         .AddJwtBearer(options =>
        //         {
        //             options.TokenValidationParameters = new TokenValidationParameters
        //             {
        //                 ValidateIssuer = true,
        //                 ValidateAudience = true,
        //                 ValidateIssuerSigningKey = true,
        //                 ValidIssuer = "Issuer", // Ganti dengan issuer yang sesuai
        //                 ValidAudience = "Audience", // Ganti dengan audience yang sesuai
        //                 IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("SecretKey")) // Ganti dengan secret key yang sesuai
        //             };
        //         });
        //     // Add services to the container.
        //     services.AddControllers();

        //     // Konfigurasi database PostgreSQL
        //     services.AddDbContext<AppDbContext>(options =>
        //         options.UseNpgsql(_configuration.GetConnectionString("DefaultConnection")));
        //     // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        //     services.AddEndpointsApiExplorer();
        //     services.AddSwaggerGen();
        //     services.Configure<JwtOptions>(_configuration.GetSection("JwtConfig"));
        // }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Konfigurasi middleware Anda di sini
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                // Configure the HTTP request pipeline.
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Vendor V1");
                    
                    // Tambahkan otorisasi di Swagger UI
                    c.RoutePrefix = string.Empty; // Atur path URL Swagger UI sesuai kebutuhan Anda
                    c.DocumentTitle = "API Vendor - Swagger UI";
                    c.EnableDeepLinking();
                    c.DisplayOperationId();
                    c.DisplayRequestDuration();
                });
            }
            app.UseMiddleware<JwtMiddleware>();

            app.UseRouting();

            // Konfigurasi routing dan endpoint Anda di sini
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseHttpsRedirection();
            app.UseAuthentication();
            app.UseAuthorization();
        }
    }
}