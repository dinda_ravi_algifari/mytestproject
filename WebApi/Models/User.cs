using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{

    public class User : BaseModel
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }

}